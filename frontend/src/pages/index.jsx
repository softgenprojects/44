import { Box, VStack, Heading, Button, Text, useColorModeValue, Center, SimpleGrid, Icon, Link } from '@chakra-ui/react';
import { BrandLogo } from '@components/BrandLogo';
import { FaBalanceScale, FaFileContract, FaGavel, FaRegThumbsUp } from 'react-icons/fa';

const Home = () => {
  const bgColor = useColorModeValue('gray.50', 'gray.800');
  const textColor = useColorModeValue('gray.800', 'whiteAlpha.900');

  return (
    <Box bg={bgColor} color={textColor} minH="100vh" py="12" px={{ base: '4', lg: '8' }}>
      <VStack spacing="8">
        <BrandLogo />
        <Heading as="h1" size="2xl" textAlign="center">
          Legal Contract Reviews
        </Heading>
        <Text textAlign="center" maxW="2xl">
          Ensure your contracts are solid and fair with our professional review services.
        </Text>
        <Button colorScheme="blue" size="lg">
          Get Started
        </Button>
      </VStack>
      <SimpleGrid columns={{ base: 1, md: 2, lg: 4 }} spacing="8" mt="16">
        <FeatureCard
          icon={FaBalanceScale}
          title="Balanced Contracts"
          description="Achieve fair terms with balanced contracts that protect all parties involved."
        />
        <FeatureCard
          icon={FaFileContract}
          title="Detailed Analysis"
          description="Our experts meticulously review every clause to safeguard your interests."
        />
        <FeatureCard
          icon={FaGavel}
          title="Legal Expertise"
          description="Leverage our legal expertise to navigate complex contract negotiations."
        />
        <FeatureCard
          icon={FaRegThumbsUp}
          title="Trusted Reviews"
          description="Count on our trusted professionals for unbiased and thorough contract reviews."
        />
      </SimpleGrid>
      <Box as="section" mt="32" textAlign="center">
        <Heading as="h2" size="xl" mb="4">
          Ready to get started?
        </Heading>
        <Text fontSize="lg" mb="8">
          Contact us today and take the first step towards secure and reliable contracts.
        </Text>
        <Link href="#" isExternal>
          <Button colorScheme="green" size="lg">
            Contact Us
          </Button>
        </Link>
      </Box>
    </Box>
  );
};

const FeatureCard = ({ icon, title, description }) => (
  <VStack spacing="4" textAlign="center">
    <Icon as={icon} boxSize="6" color="blue.500" />
    <Heading as="h3" size="md">
      {title}
    </Heading>
    <Text color="gray.600">
      {description}
    </Text>
  </VStack>
);

export default Home;
